package com.github.dwursteisen.codestory.util

import java.util
import com.github.dwursteisen.codestory.solutions.{AucuneReponseTrouve, CodeStorySolution}
import collection.convert.WrapAsScala

/**
 * Created with IntelliJ IDEA.
 * User: david
 * Date: 26/01/13
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */
class SolutionFinder {

  def giveAnswerToQuestion(question: String = "") = {
    new FromQuestionFinder(question)
  }

  class FromQuestionFinder(val question: String = "") {
    def from(solutions: util.Collection[CodeStorySolution]): String = {
      val scalaIterable: Iterable[CodeStorySolution] = WrapAsScala.collectionAsScalaIterable(solutions)
      val solution: CodeStorySolution = scalaIterable.find(_.canRespondToThisQuestion(question)).getOrElse(new AucuneReponseTrouve())
      solution.giveResponse(question)
    }
  }

}
