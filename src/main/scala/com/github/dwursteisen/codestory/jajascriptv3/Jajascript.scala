/*
 * Copyright (c) 2013. - by $user
 *
 * Pas touche qu'on vous à dis !
 */

package com.github.dwursteisen.codestory.jajascriptv3

import com.github.dwursteisen.codestory.jajascript.{Planning, Commande}
import collection.convert.{WrapAsJava, WrapAsScala}
import collection.mutable
import collection.mutable.ArrayBuffer

/**
 *
 *
 * @author dwu
 */
class Jajascript {

  class PlanningScala(val gain: Int = 0, val commandes: ArrayBuffer[Commande] = ArrayBuffer()) {

    def toPlanning: Planning = {
      new Planning(gain, WrapAsJava.seqAsJavaList(commandes.map(_.getVol)))
    }
  }

  def optimize(commandes: java.util.Collection[Commande]): Planning = {
    val scalaIterable: Iterable[Commande] = WrapAsScala.collectionAsScalaIterable(commandes)
    val meilleurPlanning = optimize(scalaIterable.toSeq)
    meilleurPlanning.toPlanning
  }

  def emptySeq: Seq[Commande] = Seq()

  def optimize(commandes: Seq[Commande]): PlanningScala = {
    val meilleur_couple_gain_commande_par_heure: mutable.HashMap[Int, (Int, Commande)] = mutable.HashMap()
    val prececedent: mutable.Map[Commande, Commande] = mutable.Map()

    val groupe_par_arrivee: Map[Int, Seq[Commande]] = commandes.groupBy(_.arrive)
    val ensemble_heures_arrivees: Seq[Int] = groupe_par_arrivee.keySet.toIndexedSeq.sortWith(_ < _)

    // trouve la durée la plus longue pour ensuite trouver le départ "au plus tôt"
    val plus_grand_depart = if (commandes.isEmpty) 0 else commandes.sortBy(-_.getDuree).head.getDuree

    var meilleur_couple_gain_commande: (Int, Commande) = (0, null)

    // pour toutes les heures trouve le meilleur planning !
    ensemble_heures_arrivees.foreach(heure => {
      var meilleure_commande_precedente_pour_cette_heure: Commande = null
      var meilleur_couple_gain_commande_pour_cette_heure: (Int, Commande) = (0, null)

      val commandes_en_arrivee: Seq[Commande] = groupe_par_arrivee.getOrElse(heure, emptySeq)

      // trouve parmis les commandes arrivé pour cette heure ci le plus grand planning
      commandes_en_arrivee.foreach(commande => {

        val heure_min_de_depart: Int = commande.getDepart - plus_grand_depart

        // trouve la meilleur combinaison entre la commande et les plannings precedents
        (heure_min_de_depart to commande.getDepart).foreach(heure_precedente => {
          val couple_precedent: (Int, Commande) = meilleur_couple_gain_commande_par_heure.getOrElse(heure_precedente, (0, null))
          val gain_cumule: Int = couple_precedent._1 + commande.getPrix
          if (gain_cumule > meilleur_couple_gain_commande_pour_cette_heure._1) {
            meilleur_couple_gain_commande_pour_cette_heure = (gain_cumule, commande)
            meilleure_commande_precedente_pour_cette_heure = couple_precedent._2
          }
        })

      })
      // garde le couple meilleur gain/commande pour cette heure
      meilleur_couple_gain_commande_par_heure.put(heure, meilleur_couple_gain_commande_pour_cette_heure)

      // garde le couple meilleur gain/commande total
      if (meilleur_couple_gain_commande_pour_cette_heure._1 > meilleur_couple_gain_commande._1) {
        meilleur_couple_gain_commande = meilleur_couple_gain_commande_pour_cette_heure
        prececedent.put(meilleur_couple_gain_commande_pour_cette_heure._2, meilleure_commande_precedente_pour_cette_heure)
      }
    })

    val path = buildPath(meilleur_couple_gain_commande._2, prececedent)
    new PlanningScala(meilleur_couple_gain_commande._1, path)
  }


  def buildPath(commande: Commande, prececedent: mutable.Map[Commande, Commande]) = {
    var meilleurCommande: Commande = commande
    val result = ArrayBuffer[Commande]()
    while (meilleurCommande != null) {
      result += meilleurCommande
      val prec: Commande = prececedent.getOrElse(meilleurCommande, null)
      prececedent.remove(meilleurCommande)
      meilleurCommande = prec
    }
    result.reverse
  }
}
