package com.github.dwursteisen.codestory.solutions;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: david
 * Date: 12/01/13
 * Time: 16:12
 * To change this template use File | Settings | File Templates.
 */
public class QuestionsAvecReponseEnDur implements CodeStorySolution {

    private final Map<String, String> questionsReponses = new HashMap<>();

    public QuestionsAvecReponseEnDur() {
        questionsReponses.put("Quelle est ton adresse email", "david.wursteisen@gmail.com");
        questionsReponses.put("As tu passe une bonne nuit malgre les bugs de l etape precedente(PAS_TOP/BOF/QUELS_BUGS)", "QUELS_BUGS");
        questionsReponses.put("Es tu pret a recevoir une enonce au format markdown par http post(OUI/NON)", "OUI");
        questionsReponses.put("Es tu heureux de participer(OUI/NON)", "OUI");
        questionsReponses.put("Es tu abonne a la mailing list(OUI/NON)", "OUI");
        questionsReponses.put("Est ce que tu reponds toujours oui(OUI/NON)", "NON");
        questionsReponses.put("As tu copie le code de ndeloof(OUI/NON/JE_SUIS_NICOLAS)", "NON");
    }

    @Override
    public boolean canRespondToThisQuestion(final String question) {
        return questionsReponses.containsKey(question);
    }

    @Override
    public String giveResponse(final String toQuestion) {
        return questionsReponses.get(toQuestion);
    }
}
