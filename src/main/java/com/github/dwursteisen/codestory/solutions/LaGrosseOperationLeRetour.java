package com.github.dwursteisen.codestory.solutions;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * User: david
 * Date: 12/01/13
 * Time: 14:13
 * <p/>
 * <p/>
 * Il faut que ça soit dit
 * <p/>
 * Cette class tue des arbres.
 * <p/>
 * En effet, elle effectue un calcul complexe complètement inutile car la réponse sera toujours 1
 * <p/>
 * Je pourrais donc répondre 1 sans réfléchir.
 * <p/>
 * <p/>
 * En plus, cette histoire m'oblige à écrire un (long) commentaire qui tue des bytes...
 * ...et peut être des arbres aussi.
 */
public class LaGrosseOperationLeRetour implements CodeStorySolution {
    private final LaGrosseOperation laGrosseOperation = new LaGrosseOperation();

    @Override
    public boolean canRespondToThisQuestion(String question) {
        return question.equals("(((1,1 2) 3,14 4 (5 6 7) (8 9 10)*4267387833344334647677634)/2*553344300034334349999000)/31878018903828899277492024491376690701584023926880");
    }

    @Override
    public String giveResponse(String toQuestion) {
        // /?q=(((1,1+2)+3,14+4+(5+6+7)+(8+9+10)*4267387833344334647677634)/2*553344300034334349999000)/31878018903828899277492024491376690701584023926880
        BigDecimal resultat = laGrosseOperation.computeTheGrosDecimal().divide(new BigDecimal("31878018903828899277492024491376690701584023926880"));
        return resultat.setScale(0, RoundingMode.HALF_DOWN).toPlainString();

    }
}
