package com.github.dwursteisen.codestory.solutions;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

import groovy.lang.GroovyShell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA. User: david Date: 23/01/13 Time: 22:39 To change this template use File | Settings | File Templates.
 */
public class CalculAvecGroovy implements CodeStorySolution {

    private static final NumberFormat FRANCE_DECIMAL_FORMATER = NumberFormat.getInstance(Locale.FRANCE);
    private static final Logger LOG = LoggerFactory.getLogger("groovy-shell");
    private final GroovyShell shell = new GroovyShell();

    @Override
    public boolean canRespondToThisQuestion(final String question) {
        return !"".equals(question.trim());
    }

    @Override
    public String giveResponse(final String toQuestion) {
        try {
            String groovyOperation = toQuestion.replace(",", ".").replace(" ", "+");
            BigDecimal result = new BigDecimal(String.valueOf(shell.evaluate("def q=" + groovyOperation)));

            if (containsDecimals(result)) {
                return FRANCE_DECIMAL_FORMATER.format(result);
            } else {
                return result.setScale(0, RoundingMode.CEILING).toString();
            }
        } catch (NumberFormatException ex) {
            LOG.warn("Problème en essayant de gérer l'opération {}. Veuillez vérifier si c'est une opération arithmétique valide.",
                    toQuestion, ex);
            return "";
        }
    }

    private boolean containsDecimals(final BigDecimal result) {
        return result.stripTrailingZeros().scale() != 0;
    }
}
