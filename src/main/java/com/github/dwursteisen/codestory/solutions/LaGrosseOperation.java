package com.github.dwursteisen.codestory.solutions;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * User: david
 * Date: 12/01/13
 * Time: 13:08
 */
public class LaGrosseOperation implements CodeStorySolution {
    @Override
    public boolean canRespondToThisQuestion(String question) {
        return question.equals("((1,1 2) 3,14 4 (5 6 7) (8 9 10)*4267387833344334647677634)/2*553344300034334349999000");
    }

    @Override
    public String giveResponse(String toQuestion) {
        BigDecimal resultat = computeTheGrosDecimal();
        return resultat.setScale(0, RoundingMode.HALF_DOWN).toPlainString();
    }

    BigDecimal computeTheGrosDecimal() {
        // ((1,1+2)+3,14+4+(5+6+7)+(8+9+10)*4267387833344334647677634)/2*553344300034334349999000
        BigDecimal parenthese1 = new BigDecimal("1.1").add(new BigDecimal("2"));
        BigDecimal parenthese2 = new BigDecimal("5").add(new BigDecimal("6")).add(new BigDecimal("7"));
        BigDecimal parenthese3 = new BigDecimal("8").add(new BigDecimal("9")).add(new BigDecimal("10"));
        BigDecimal premierProduit = parenthese3.multiply(new BigDecimal("4267387833344334647677634"));

        BigDecimal grosseParenthese = parenthese1.add(new BigDecimal("3.14")).add(new BigDecimal("4")).add(parenthese2).add(premierProduit);
        return grosseParenthese.divide(new BigDecimal("2")).multiply(new BigDecimal("553344300034334349999000"));
    }
}
