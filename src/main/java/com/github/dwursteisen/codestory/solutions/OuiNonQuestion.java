package com.github.dwursteisen.codestory.solutions;

public class OuiNonQuestion implements CodeStorySolution {
    @Override
    public boolean canRespondToThisQuestion(final String question) {
        return question.contains("(OUI/NON)");
    }

    @Override
    public String giveResponse(final String toQuestion) {
        return "OUI";
    }
}
