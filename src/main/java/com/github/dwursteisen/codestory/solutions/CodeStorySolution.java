package com.github.dwursteisen.codestory.solutions;

public interface CodeStorySolution {

    boolean canRespondToThisQuestion(final String question);

    String giveResponse(final String toQuestion);
}
