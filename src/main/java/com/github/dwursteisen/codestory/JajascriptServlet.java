package com.github.dwursteisen.codestory;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.dwursteisen.codestory.jajascript.Commande;
import com.github.dwursteisen.codestory.jajascript.Planning;
import com.github.dwursteisen.codestory.jajascriptv3.Jajascript;
import com.github.dwursteisen.codestory.util.Timer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA. User: david Date: 13/01/13 Time: 20:36 To change this template use File | Settings | File Templates.
 */
public class JajascriptServlet extends HttpServlet {

    private final Jajascript jajascript = new Jajascript();
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    protected void service(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        Timer t = new Timer();
        t.mesure("EXTRACTION JSON");
        Collection<Commande> commandes = readJson(request);

        t.mesure(String.format("LECTURE JSON \t\t(%d commandes)", commandes.size()));
        Planning planning = jajascript.optimize(commandes);
        t.mesure(String.format("OPTIMIZATION \t\t(%d de gain)", planning.getGain()));

        t.mesure("ECRITURE JSON");
        response.setStatus(HttpServletResponse.SC_CREATED);
        response.setContentType("application/json");
        writeJson(response, planning);
        t.mesure("ENVOI REPONSE");

    }

    private Collection<Commande> readJson(HttpServletRequest request) throws IOException {
        if (request.getContentLength() <= 0) {
            return new ArrayList<>();
        }
        JsonParser jp = new JsonFactory().createJsonParser(request.getInputStream());
        Collection<Commande> commandes = mapper.readValue(jp, mapper.getTypeFactory().constructCollectionType(LinkedList.class, Commande.class));
        return commandes;
    }

    private void writeJson(final HttpServletResponse response, final Planning planning) throws IOException {
        mapper.writeValue(response.getOutputStream(), planning);
    }

}
