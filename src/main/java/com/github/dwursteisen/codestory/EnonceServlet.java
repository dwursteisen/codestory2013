package com.github.dwursteisen.codestory;

import com.github.dwursteisen.codestory.util.QueryExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EnonceServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger("enonce");
    private final QueryExtractor queryExtractor = new QueryExtractor();

    @Override
    protected void service(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        LOG.info("Enonce reçu => {}", queryExtractor.extractFrom(request).postContent());
        response.setStatus(HttpServletResponse.SC_CREATED);
    }
}
