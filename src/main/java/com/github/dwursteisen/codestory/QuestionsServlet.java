package com.github.dwursteisen.codestory;

import com.github.dwursteisen.codestory.solutions.*;
import com.github.dwursteisen.codestory.util.QueryExtractor;
import com.github.dwursteisen.codestory.util.SolutionFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class QuestionsServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger("queries");
    private final SolutionFinder finder = new SolutionFinder();
    private final QueryExtractor extractor = new QueryExtractor();
    private final List<CodeStorySolution> solutions = Arrays.asList(
            new QuestionsAvecReponseEnDur(),
            new LaGrosseOperation(),
            new LaGrosseOperationLeRetour(),
            new OuiNonQuestion(),
            new CalculAvecGroovy());

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String questionFromOuterSpace = extractor.extractFrom(request).parameter("q");
        String solutionResponse = finder.giveAnswerToQuestion(questionFromOuterSpace).from(solutions);
        LOG.info("Pour la question [{}] ; la réponse devrait être [{}]", questionFromOuterSpace, solutionResponse);
        response.getWriter().print(solutionResponse);
    }

}
