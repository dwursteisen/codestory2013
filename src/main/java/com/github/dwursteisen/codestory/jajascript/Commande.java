package com.github.dwursteisen.codestory.jajascript;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Created with IntelliJ IDEA. User: david Date: 14/01/13 Time: 01:28 To change this template use File | Settings | File Templates.
 */
public class Commande {
    @SerializedName("VOL")
    private final String vol;
    @SerializedName("DEPART")
    private final int depart;
    @SerializedName("DUREE")
    private final int duree;
    @SerializedName("PRIX")
    private final int prix;

    @JsonCreator
    public Commande(@JsonProperty(value = "VOL") final String vol, @JsonProperty(value = "DEPART") final int depart,
                    @JsonProperty(value = "DUREE") final int duree, @JsonProperty(value = "PRIX") final int prix) {
        this.vol = vol;
        this.depart = depart;
        this.duree = duree;
        this.prix = prix;
    }

    public String getVol() {
        return vol;
    }

    public int getDepart() {
        return depart;
    }

    public int getDuree() {
        return duree;
    }

    public int getPrix() {
        return prix;
    }

    public int arrive() {
        return depart + duree;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Commande commande = (Commande) o;

        if (!vol.equals(commande.vol))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return vol.hashCode();
    }


    @Override
    public String toString() {
        return vol + "[" + depart + "->" + (depart + duree) + "]:" + prix + " ";
    }

}
