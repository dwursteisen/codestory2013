package com.github.dwursteisen.codestory.jajascript;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: david
 * Date: 14/01/13
 * Time: 01:29
 * To change this template use File | Settings | File Templates.
 */
public class Planning {
    private final int gain;
    private final List<String> path;

    public Planning(final int gain, final List<String> path) {
        this.gain = gain;
        this.path = path;
    }

    public int getGain() {
        return gain;
    }

    public List<String> getPath() {
        return path;
    }
}
