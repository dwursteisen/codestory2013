package com.github.dwursteisen.codestory.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

public class LogEveryInput implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger("input-filter");

    public static StringBuilder extractReadableParametersForLogging(final HttpServletRequest httpRequest) {
        StringBuilder parametersWithValuePrintable = new StringBuilder();
        for (Map.Entry<String, Object[]> parameter : (Set<Map.Entry>) httpRequest.getParameterMap().entrySet()) {

            parametersWithValuePrintable.append(parameter.getKey()).append("->");
            parametersWithValuePrintable.append(Arrays.toString(parameter.getValue()));
            parametersWithValuePrintable.append(" ; ");

        }
        return parametersWithValuePrintable;
    }

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException,
            ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        // jajascript exclusion
        if (((HttpServletRequest) request).getServletPath().startsWith("/jajascript")) {
            chain.doFilter(request, response);
            return;
        }

        StringBuilder parametersWithValuePrintable = extractReadableParametersForLogging(httpRequest);
        LOG.info("uri: {} | parameters : {}", httpRequest.getRequestURI(), parametersWithValuePrintable);

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
