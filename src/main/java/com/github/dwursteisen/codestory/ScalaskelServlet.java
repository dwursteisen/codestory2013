package com.github.dwursteisen.codestory;

import com.github.dwursteisen.codestory.util.QueryExtractor;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ScalaskelServlet extends HttpServlet {

    private static final int BAR = 7;
    private static final int QIX = 11;
    private static final int BAZ = 21;
    private static final Logger LOG = LoggerFactory.getLogger("queries");
    private final QueryExtractor queryExtractor = new QueryExtractor();

    @Override
    protected void service(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        final int value = queryExtractor.extractFrom(request).uriPatern("/scalaskel/change/(\\d+)").asInt();

        List<Denomination> allDenominations = computationAllDenominations(value);
        List<Denomination> validDenominations = filterValidDenominations(value, allDenominations);

        String json = new Gson().toJson(validDenominations);
        LOG.info("Pour la valeur {} je vais produire le json {}", value, json);
        response.setContentType("application/json");
        response.getWriter().print(json);

    }

    private List<Denomination> filterValidDenominations(final int value, final List<Denomination> allDenominations) {
        return FluentIterable.from(allDenominations).filter(new Predicate<Denomination>() {
            @Override
            public boolean apply(final Denomination denomination) {
                return denomination.isValid(value);
            }
        }).toList();
    }

    private List<Denomination> computationAllDenominations(final int value) {
        List<Denomination> allDenomination = new ArrayList<>();

        for (int nbBaz = 0; nbBaz <= value / BAZ; nbBaz++) {
            for (int nbQix = 0; nbQix <= value / QIX; nbQix++) {
                for (int nbBar = 0; nbBar <= value / BAR; nbBar++) {
                    for (int nbFoo = 0; nbFoo <= value; nbFoo++) {
                        Denomination denomination = new Denomination(nbFoo, nbBar, nbQix, nbBaz);
                        allDenomination.add(denomination);
                    }
                }

            }
        }
        return allDenomination;
    }

    private static class Denomination {
        private final Integer foo;
        private final Integer bar;
        private final Integer qix;
        private final Integer baz;

        public Denomination(int foo, int bar, int qix, int baz) {
            this.foo = foo != 0 ? foo : null;
            this.bar = bar != 0 ? bar : null;
            this.qix = qix != 0 ? qix : null;
            this.baz = baz != 0 ? baz : null;
        }

        public boolean isValid(int value) {
            value -= foo != null ? foo : 0;
            value -= bar != null ? bar * BAR : 0;
            value -= qix != null ? qix * QIX : 0;
            value -= baz != null ? baz * BAZ : 0;

            return value == 0;

        }

    }
}
