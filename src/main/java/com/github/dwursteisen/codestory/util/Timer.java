package com.github.dwursteisen.codestory.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author dwu
 */
public class Timer {
    private static final Logger LOG = LoggerFactory.getLogger("timer");
    long now = -1;

    public void mesure(String message) {
        if (now == -1) {
            now = System.currentTimeMillis();
            LOG.info("0000000 \t\t\t\t{}", message);
            return;
        }
        long nextNow = System.currentTimeMillis();
        LOG.info("{}sec \t\t\t\t{}", (nextNow - now) / 1000, message);
        now = nextNow;
    }

}
