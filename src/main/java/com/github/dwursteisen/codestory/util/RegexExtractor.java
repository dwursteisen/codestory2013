package com.github.dwursteisen.codestory.util;

import java.math.BigDecimal;
import java.util.regex.Matcher;

/**
 * Created with IntelliJ IDEA.
 * User: david
 * Date: 11/01/13
 * Time: 20:40
 * To change this template use File | Settings | File Templates.
 */
public class RegexExtractor {
    private final Matcher matcher;

    public RegexExtractor(final Matcher matcher) {
        this.matcher = matcher;
    }

    public Converter theGroup(final int groupNumber) {
        return new Converter(matcher.group(groupNumber + 1));
    }

    public static class Converter {

        private final String value;

        public Converter(final String value) {
            this.value = value;
        }

        public int asInt() {
            return Integer.parseInt(value);
        }

        public String asString() {
            return value;
        }

        public BigDecimal asBigDecimal() {
            return new BigDecimal(value);
        }

        public double asDouble() {
            return Double.parseDouble(value);
        }
    }
}
