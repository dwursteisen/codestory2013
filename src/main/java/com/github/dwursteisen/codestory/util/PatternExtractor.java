package com.github.dwursteisen.codestory.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: david
 * Date: 11/01/13
 * Time: 20:18
 * To change this template use File | Settings | File Templates.
 */
public class PatternExtractor {
    public StringExtractor extractFrom(final String stringToBeExtracted) {
        return new StringExtractor(stringToBeExtracted);
    }

    public static class StringExtractor {
        private final String stringToBeExtracted;

        StringExtractor(final String stringToBeExtracted) {
            this.stringToBeExtracted = stringToBeExtracted;
        }

        public RegexExtractor withPattern(final String pattern) {
            Matcher matcher = Pattern.compile(pattern).matcher(stringToBeExtracted);
            matcher.find();
            return new RegexExtractor(matcher);
        }
    }

}
