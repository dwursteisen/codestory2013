package com.github.dwursteisen.codestory.util;

import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class QueryExtractor {

    public FromHttpRequestExtractor extractFrom(final HttpServletRequest httpRequest) {
        return new FromHttpRequestExtractor(httpRequest);
    }

    public static class FromHttpRequestExtractor {
        private final HttpServletRequest httpRequest;

        FromHttpRequestExtractor(final HttpServletRequest httpRequest) {
            this.httpRequest = httpRequest;
        }

        public String parameter(final String parameterName) {
            if (httpRequest == null) {
                return "";
            }
            String parameterFromRequest = httpRequest.getParameter(parameterName);
            if (parameterFromRequest == null) {
                return "";
            }
            return parameterFromRequest;
        }

        public RegexExtractor.Converter uriPatern(final String pattern) {
            String uri = httpRequest.getRequestURI();
            return new PatternExtractor().extractFrom(uri).withPattern(pattern).theGroup(0);
        }

        public String postContent() {
            try {
                return IOUtils.toString(httpRequest.getInputStream());
            } catch (IOException e) {
                return "";
            }
        }
    }
}
