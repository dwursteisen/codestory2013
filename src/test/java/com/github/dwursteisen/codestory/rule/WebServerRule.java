package com.github.dwursteisen.codestory.rule;

import com.github.dwursteisen.codestory.EnonceServlet;
import com.github.dwursteisen.codestory.JajascriptServlet;
import com.github.dwursteisen.codestory.QuestionsServlet;
import com.github.dwursteisen.codestory.ScalaskelServlet;
import com.github.dwursteisen.codestory.filter.LogEveryInput;
import org.eclipse.jetty.server.DispatcherType;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.EnumSet;
import java.util.Random;

/**
 * @author <a href="mailto:nicolas.deloof@gmail.com">Nicolas De Loof</a>
 */
public class WebServerRule implements TestRule {

    private final int portNumber;

    public WebServerRule() {
        this.portNumber = Integer.getInteger("port", 8080 + new Random().nextInt(200));
    }

    public int getPort() {
        return portNumber;
    }

    @Override
    public Statement apply(final Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                Server server = new Server(portNumber);
                ServletContextHandler context = new ServletContextHandler();
                context.setContextPath("/");
                server.setHandler(context);
                context.addFilter(new FilterHolder(new LogEveryInput()), "/*", EnumSet.allOf(DispatcherType.class));
                context.addServlet(new ServletHolder(new EnonceServlet()), "/enonce/*");
                context.addServlet(new ServletHolder(new ScalaskelServlet()), "/scalaskel/change/*");
                context.addServlet(new ServletHolder(new JajascriptServlet()), "/jajascript/optimize");
                context.addServlet(new ServletHolder(new QuestionsServlet()), "/*");

                server.start();
                try {
                    base.evaluate();
                } finally {
                    server.stop();
                }
            }
        };
    }
}
