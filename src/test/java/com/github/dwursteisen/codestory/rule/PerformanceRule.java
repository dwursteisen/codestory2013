package com.github.dwursteisen.codestory.rule;


import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import static junit.framework.TestCase.fail;


/**
 * Created with IntelliJ IDEA.
 * User: david
 * Date: 27/01/13
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */
public class PerformanceRule implements TestRule {

    private final int limitInMilliSeconds;

    public PerformanceRule(int limitInMilliSeconds) {
        this.limitInMilliSeconds = limitInMilliSeconds;
    }

    @Override
    public Statement apply(final Statement statement, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                long now = System.currentTimeMillis();
                statement.evaluate();
                long timing = System.currentTimeMillis() - now;
                if (timing > limitInMilliSeconds) {
                    fail("Le test est trop long ! Il de passe la limit de " + limitInMilliSeconds + " millisecondes");
                }
            }
        };


    }
}
