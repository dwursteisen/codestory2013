package com.github.dwursteisen.codestory.util;

import org.fest.assertions.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

@RunWith(MockitoJUnitRunner.class)
public class QueryExtractorTest {

    @Mock
    private HttpServletRequest httpRequest;
    private QueryExtractor queryExtractor;

    @Before
    public void setUp() throws Exception {
        queryExtractor = new QueryExtractor();
    }

    @Test
    public void should_extract_question_from_query() {
        doReturn("My question is...").when(httpRequest).getParameter("q");
        String question = queryExtractor.extractFrom(httpRequest).parameter("q");
        assertThat(question).isEqualTo("My question is...");
    }

    @Test
    public void should_return_empty_string_without_parameter() {
        doReturn("My question is...").when(httpRequest).getParameter("q");
        String question = queryExtractor.extractFrom(httpRequest).parameter(null);
        assertThat(question).isEmpty();
    }

    // badass test
    @Test
    public void should_return_empty_string_without_http_request() {
        String question = queryExtractor.extractFrom(null).parameter("q");
        assertThat(question).isEmpty();
    }

    @Test
    public void should_return_empty_string_without_post_content() throws IOException {
        doReturn(new MockServletInputStream("")).when(httpRequest).getInputStream();
        String question = queryExtractor.extractFrom(httpRequest).postContent();
        assertThat(question).isEmpty();
    }

    @Test
    public void should_extract_post_content() throws IOException {
        doReturn(new MockServletInputStream("hello world !")).when(httpRequest).getInputStream();
        String question = queryExtractor.extractFrom(httpRequest).postContent();
        assertThat(question).isEqualTo("hello world !");
    }

    @Test
    public void should_return_empty_string_when_ioexception() throws IOException {
        doThrow(new IOException("* boom *")).when(httpRequest).getInputStream();
        String question = queryExtractor.extractFrom(httpRequest).postContent();
        assertThat(question).isEmpty();
    }

    private static class MockServletInputStream extends ServletInputStream {

        private final ByteArrayInputStream delegateInputstream;

        private MockServletInputStream(String content) {
            delegateInputstream = new ByteArrayInputStream(content.getBytes());
        }

        @Override
        public int read() throws IOException {
            return delegateInputstream.read();
        }
    }

    @Test
    public void should_extract_resource_id() {
        doReturn("/scalaskel/change/456").when(httpRequest).getRequestURI();
        int id = queryExtractor.extractFrom(httpRequest).uriPatern("/scalaskel/change/(\\d+)").asInt();
        Assertions.assertThat(id).isEqualTo(456);
    }

}
