package com.github.dwursteisen.codestory.util;

import com.github.dwursteisen.codestory.solutions.CodeStorySolution;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;

public class SolutionFinderTest {

    private final SolutionFinder solutionFinder = new SolutionFinder();


    @Test
    public void should_give_the_answer() {
        CodeStorySolution expectedSolution = new MyCodeStorySolution();
        List<CodeStorySolution> whatIsMyNameSolution = Arrays.asList(expectedSolution);
        assertThat(solutionFinder.giveAnswerToQuestion("What is my name").from(whatIsMyNameSolution)).isEqualTo("John Doe");
    }

    private static class MyCodeStorySolution implements CodeStorySolution {
        @Override
        public boolean canRespondToThisQuestion(final String question) {
            return "What is my name".equals(question);
        }

        @Override
        public String giveResponse(final String toQuestion) {
            return "John Doe";
        }
    }
}
