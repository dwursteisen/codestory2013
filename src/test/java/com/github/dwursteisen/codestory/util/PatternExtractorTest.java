package com.github.dwursteisen.codestory.util;

import org.fest.assertions.api.Assertions;
import org.junit.Test;

import java.math.BigDecimal;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: david
 * Date: 11/01/13
 * Time: 20:20
 * To change this template use File | Settings | File Templates.
 */
public class PatternExtractorTest {

    @Test
    public void should_extract_numbers_with_pattern() {
        RegexExtractor extract = new PatternExtractor().extractFrom("50 + 20").withPattern("(\\d+) \\+ (\\d+)");
        assertThat(extract.theGroup(0).asInt()).isEqualTo(50);
        assertThat(extract.theGroup(1).asInt()).isEqualTo(20);
    }

    @Test
    public void should_extract_string_with_pattern() {
        RegexExtractor extract = new PatternExtractor().extractFrom("(50 3)*20").withPattern("\\(([\\w ]*)\\)\\*(\\d+)");
        Assertions.assertThat(extract.theGroup(0).asString()).isEqualTo("50 3");

    }

    @Test
    public void should_extract_bigDecimal_with_pattern() {
        BigDecimal result = new PatternExtractor().extractFrom("3").withPattern("(\\d)").theGroup(0).asBigDecimal();
        Assertions.assertThat(result).isEqualTo("3");
    }
}
