/*
 * Copyright (c) 2013. - by $user
 *
 * Pas touche qu'on vous à dis !
 */

package com.github.dwursteisen.codestory.jajascript;

import com.github.dwursteisen.codestory.jajascriptv3.Jajascript;
import com.github.dwursteisen.codestory.rule.PerformanceRule;
import com.github.dwursteisen.codestory.rule.WebServerRule;
import com.github.dwursteisen.codestory.util.Timer;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * @author dwu
 */
public class JajascriptPerformanceTest {

    private static final String EXEMPLE_LONG = " [{\"VOL\":\"unsightly-interpreter-53\",\"DEPART\":0,\"DUREE\":4,\"PRIX\":8},{\"VOL\":\"grotesque-corn-7\",\"DEPART\":1,\"DUREE\":2,\"PRIX\":9},{\"VOL\":\"careful-semicircle-81\",\"DEPART\":2,\"DUREE\":6,\"PRIX\":3},{\"VOL\":\"colorful-paw-54\",\"DEPART\":4,\"DUREE\":5,\"PRIX\":15},{\"VOL\":\"famous-washbasin-74\",\"DEPART\":5,\"DUREE\":2,\"PRIX\":12},{\"VOL\":\"voiceless-pie-86\",\"DEPART\":5,\"DUREE\":4,\"PRIX\":14},{\"VOL\":\"wicked-blackboard-30\",\"DEPART\":6,\"DUREE\":2,\"PRIX\":1},{\"VOL\":\"crazy-hairstyle-41\",\"DEPART\":7,\"DUREE\":6,\"PRIX\":2},{\"VOL\":\"light-seacoast-44\",\"DEPART\":9,\"DUREE\":5,\"PRIX\":6},{\"VOL\":\"kind-square-44\",\"DEPART\":10,\"DUREE\":2,\"PRIX\":18},{\"VOL\":\"old-fashioned-sluggishness-99\",\"DEPART\":10,\"DUREE\":4,\"PRIX\":7},{\"VOL\":\"sore-grindstone-79\",\"DEPART\":11,\"DUREE\":2,\"PRIX\":10},{\"VOL\":\"huge-extinguisher-72\",\"DEPART\":12,\"DUREE\":6,\"PRIX\":1},{\"VOL\":\"lucky-trigger-71\",\"DEPART\":14,\"DUREE\":5,\"PRIX\":9},{\"VOL\":\"attractive-form-22\",\"DEPART\":15,\"DUREE\":2,\"PRIX\":20},{\"VOL\":\"young-topic-12\",\"DEPART\":15,\"DUREE\":4,\"PRIX\":9},{\"VOL\":\"combative-beetle-63\",\"DEPART\":16,\"DUREE\":2,\"PRIX\":8},{\"VOL\":\"defiant-mafioso-19\",\"DEPART\":17,\"DUREE\":6,\"PRIX\":5},{\"VOL\":\"lonely-sodium-64\",\"DEPART\":19,\"DUREE\":5,\"PRIX\":22},{\"VOL\":\"teeny-tiny-eraser-21\",\"DEPART\":20,\"DUREE\":2,\"PRIX\":28},{\"VOL\":\"blue-spectacle-63\",\"DEPART\":20,\"DUREE\":4,\"PRIX\":14},{\"VOL\":\"cruel-bun-63\",\"DEPART\":21,\"DUREE\":2,\"PRIX\":6},{\"VOL\":\"fantastic-ceremony-58\",\"DEPART\":22,\"DUREE\":6,\"PRIX\":3},{\"VOL\":\"Early-hardwood-14\",\"DEPART\":24,\"DUREE\":5,\"PRIX\":21},{\"VOL\":\"expensive-shooter-21\",\"DEPART\":25,\"DUREE\":2,\"PRIX\":24},{\"VOL\":\"lucky-moth-99\",\"DEPART\":25,\"DUREE\":4,\"PRIX\":8},{\"VOL\":\"inexpensive-moron-27\",\"DEPART\":26,\"DUREE\":2,\"PRIX\":5},{\"VOL\":\"gentle-dresser-36\",\"DEPART\":27,\"DUREE\":6,\"PRIX\":4},{\"VOL\":\"panicky-root-81\",\"DEPART\":29,\"DUREE\":5,\"PRIX\":15},{\"VOL\":\"square-podium-63\",\"DEPART\":30,\"DUREE\":2,\"PRIX\":26},{\"VOL\":\"misty-toupee-77\",\"DEPART\":30,\"DUREE\":4,\"PRIX\":12},{\"VOL\":\"steep-sticker-67\",\"DEPART\":31,\"DUREE\":2,\"PRIX\":5},{\"VOL\":\"strange-kite-42\",\"DEPART\":32,\"DUREE\":6,\"PRIX\":5},{\"VOL\":\"tame-syrup-54\",\"DEPART\":34,\"DUREE\":5,\"PRIX\":15},{\"VOL\":\"straight-skeptic-45\",\"DEPART\":35,\"DUREE\":2,\"PRIX\":23},{\"VOL\":\"itchy-temper-1\",\"DEPART\":35,\"DUREE\":4,\"PRIX\":14},{\"VOL\":\"tough-fridge-9\",\"DEPART\":36,\"DUREE\":2,\"PRIX\":1},{\"VOL\":\"exuberant-referee-53\",\"DEPART\":37,\"DUREE\":6,\"PRIX\":1},{\"VOL\":\"joyous-mud-92\",\"DEPART\":39,\"DUREE\":5,\"PRIX\":4},{\"VOL\":\"fast-slacker-60\",\"DEPART\":40,\"DUREE\":2,\"PRIX\":3},{\"VOL\":\"adorable-tin-24\",\"DEPART\":40,\"DUREE\":4,\"PRIX\":6},{\"VOL\":\"melodic-vegan-12\",\"DEPART\":41,\"DUREE\":2,\"PRIX\":7},{\"VOL\":\"high-pitched-tractor-60\",\"DEPART\":42,\"DUREE\":6,\"PRIX\":2},{\"VOL\":\"brainy-mountaineer-62\",\"DEPART\":44,\"DUREE\":5,\"PRIX\":21},{\"VOL\":\"unusual-jar-55\",\"DEPART\":45,\"DUREE\":2,\"PRIX\":24},{\"VOL\":\"petite-anchor-65\",\"DEPART\":45,\"DUREE\":4,\"PRIX\":10},{\"VOL\":\"melodic-ketchup-98\",\"DEPART\":46,\"DUREE\":2,\"PRIX\":5},{\"VOL\":\"envious-reader-51\",\"DEPART\":47,\"DUREE\":6,\"PRIX\":2},{\"VOL\":\"clumsy-tobacco-59\",\"DEPART\":49,\"DUREE\":5,\"PRIX\":23},{\"VOL\":\"easy-woman-41\",\"DEPART\":50,\"DUREE\":2,\"PRIX\":29}]";

    @ClassRule
    public static WebServerRule server = new WebServerRule();

    @Rule
    public PerformanceRule perfChecker = new PerformanceRule(30000);

    @Before
    public void setUp() {
        urlEncodingEnabled = false;
        port = server.getPort();

    }

    @Test
    public void should_be_fast_even_with_lots_of_commandes() throws Exception {
        Timer t = new Timer();
        t.mesure("début création commandes");
        List<Commande> commandes = new LinkedList<>();
        Random random = new Random();
        for (int i = 0; i < 100000; i++) {
            commandes.add(new Commande("vol-" + random.nextInt(10000), random.nextInt(2400), random.nextInt(24), random.nextInt(10)));
        }
        t.mesure("début optimize");
        Planning planning = new Jajascript().optimize(commandes);
        t.mesure("fin  optimize => gain trouvé de " + planning.getGain());
    }

    @Test
    public void should_be_polite_be_efficiant_be_fast() {
        expect().body("gain", equalTo(223)).statusCode(201).given().body(EXEMPLE_LONG).when().post("/jajascript/optimize");
    }

    @Test
    public void should_be_fast_with_some_random_commandes() {
        List<Commande> commandes = new LinkedList<>();
        Random random = new Random();
        for (int i = 0; i < 1500; i++) {
            commandes.add(new Commande("vol-" + random.nextInt(10000), random.nextInt(24), random.nextInt(24), random.nextInt(10)));
        }
        String json = new Gson().toJson(commandes);
        expect().statusCode(201).given().parameter(json, "").when().post("/jajascript/optimize");
    }
}
