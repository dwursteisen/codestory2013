/*
 * Copyright (c) 2013. - by $user
 *
 * Pas touche qu'on vous à dis !
 */

package com.github.dwursteisen.codestory.jajascript;

import com.github.dwursteisen.codestory.jajascriptv3.Jajascript;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.*;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * @author dwu
 */
public class JajascriptTest {


    public static final Type LIST_TYPE = new TypeToken<LinkedList<Commande>>() {
    }.getType();

    public Jajascript jajascript = new Jajascript();

    @Test
    public void should_optimize_one_branch() {
        List<Commande> commandes = Arrays.asList(new Commande("VOL1", 0, 5, 10), new Commande("VOL2", 5, 10, 20), new Commande("VOL3", 15,
                15, 5));
        Planning planning = jajascript.optimize(commandes);
        assertThat(planning.getGain()).isEqualTo(35);
        assertThat(planning.getPath()).containsExactly("VOL1", "VOL2", "VOL3");
    }

    @Test
    public void should_optimize_two_branches() {
        List<Commande> commandes = Arrays.asList(new Commande("VOL1", 0, 5, 10), new Commande("VOL2", 3, 10, 20), new Commande("VOL3", 13,
                15, 5));
        Planning planning = jajascript.optimize(commandes);
        assertThat(planning.getGain()).isEqualTo(25);
        assertThat(planning.getPath()).containsExactly("VOL2", "VOL3");
    }

    @Test
    public void should_optimize_two_branches_bis() {

        List<Commande> commandes = Arrays.asList(new Commande("VOL1", 1, 5, 18), new Commande("VOL2", 4, 8, 7), new Commande("VOL3", 2, 6,
                2), new Commande("VOL4", 1, 1, 14), new Commande("VOL5", 1, 17, 6));
        Planning planning = jajascript.optimize(commandes);
        assertThat(planning.getGain()).isEqualTo(21);
        assertThat(planning.getPath()).containsExactly("VOL4", "VOL2");
    }

    @Test
    public void should_optimize_circles() {
        List<Commande> commandes = Arrays.asList(new Commande("VOL1", 0, 5, 10), new Commande("VOL2", 5, 10, 10), new Commande("VOL3", 5,
                13, 5), new Commande("VOL5", 15, 5, 10), new Commande("VOL6", 20, 5, 10));

        Planning planning = jajascript.optimize(commandes);
        assertThat(planning.getGain()).isEqualTo(40);
        assertThat(planning.getPath()).containsExactly("VOL1", "VOL2", "VOL5", "VOL6");
    }

    @Test
    public void should_optimize_same_last_departure_date() {
        List<Commande> commandes = Arrays.asList(new Commande("VOL1", 0, 5, 10), new Commande("VOL2", 3, 7, 10), new Commande("VOL3", 5, 9,
                8), new Commande("VOL3", 5, 9, 7));

        Planning planning = jajascript.optimize(commandes);
        assertThat(planning.getGain()).isEqualTo(18);
        assertThat(planning.getPath()).containsExactly("VOL1", "VOL3");
    }

    @Test
    public void should_optimize_same_departure_date() {
        List<Commande> commandes = Arrays.asList(new Commande("VOL1", 0, 5, 10), new Commande("VOL2", 0, 5, 11));

        Planning planning = jajascript.optimize(commandes);
        assertThat(planning.getGain()).isEqualTo(11);
        assertThat(planning.getPath()).containsExactly("VOL2");
    }

    @Test
    public void should_optimize_real_exemple() {
        String json = "[ { \"VOL\": \"cloudy-manager-96\", \"DEPART\": 1, \"DUREE\": 5, \"PRIX\": 18 }, { \"VOL\": \"easy-pantsuit-73\", \"DEPART\": 4, \"DUREE\": 8, \"PRIX\": 7 }, { \"VOL\": \"fat-dolphin-26\", \"DEPART\": 2, \"DUREE\": 6, \"PRIX\": 2 }, { \"VOL\": \"rapid-girl-63\", \"DEPART\": 1, \"DUREE\": 1, \"PRIX\": 14 }, { \"VOL\": \"naughty-teamwork-15\", \"DEPART\": 1, \"DUREE\": 17, \"PRIX\": 6 } ]";
        Collection<Commande> commandes = readJson(json);
        Planning planning = jajascript.optimize(commandes);
        assertThat(planning.getGain()).isEqualTo(21);
        assertThat(planning.getPath()).containsExactly("rapid-girl-63", "easy-pantsuit-73");
    }

    @Test
    public void should_optimize_real_exemple_bis() {
        String json = "[ { \"VOL\": \"tiny-sender-79\", \"DEPART\": 2, \"DUREE\": 5, \"PRIX\": 27 }, { \"VOL\": \"tender-creature-21\", \"DEPART\": 3, \"DUREE\": 3, \"PRIX\": 19 }, { \"VOL\": \"impossible-wedding-86\", \"DEPART\": 2, \"DUREE\": 3, \"PRIX\": 9 }, { \"VOL\": \"dangerous-nachos-66\", \"DEPART\": 2, \"DUREE\": 6, \"PRIX\": 6 }, { \"VOL\": \"concerned-goatee-28\", \"DEPART\": 2, \"DUREE\": 9, \"PRIX\": 4 }, { \"VOL\": \"high-pitched-ivy-96\", \"DEPART\": 7, \"DUREE\": 10, \"PRIX\": 16 }, { \"VOL\": \"adventurous-polygraph-10\", \"DEPART\": 5, \"DUREE\": 8, \"PRIX\": 8 }, { \"VOL\": \"shiny-apathetic-76\", \"DEPART\": 7, \"DUREE\": 5, \"PRIX\": 5 }, { \"VOL\": \"awful-chemist-81\", \"DEPART\": 9, \"DUREE\": 2, \"PRIX\": 10 }, { \"VOL\": \"bad-theory-99\", \"DEPART\": 7, \"DUREE\": 7, \"PRIX\": 1 }, { \"VOL\": \"cruel-license-10\", \"DEPART\": 13, \"DUREE\": 5, \"PRIX\": 1 }, { \"VOL\": \"healthy-clergy-57\", \"DEPART\": 13, \"DUREE\": 5, \"PRIX\": 14 }, { \"VOL\": \"alert-transparency-75\", \"DEPART\": 10, \"DUREE\": 3, \"PRIX\": 4 }, { \"VOL\": \"hungry-fastfood-15\", \"DEPART\": 12, \"DUREE\": 1, \"PRIX\": 13 }, { \"VOL\": \"modern-rubberstamp-4\", \"DEPART\": 10, \"DUREE\": 7, \"PRIX\": 3 }, { \"VOL\": \"happy-windpipe-93\", \"DEPART\": 17, \"DUREE\": 1, \"PRIX\": 13 }, { \"VOL\": \"small-meadowlark-59\", \"DEPART\": 18, \"DUREE\": 6, \"PRIX\": 22 }, { \"VOL\": \"elegant-eyelash-51\", \"DEPART\": 15, \"DUREE\": 10, \"PRIX\": 8 }, { \"VOL\": \"dizzy-rattle-85\", \"DEPART\": 15, \"DUREE\": 9, \"PRIX\": 13 }, { \"VOL\": \"healthy-female-1\", \"DEPART\": 19, \"DUREE\": 16, \"PRIX\": 5 } ]";
        Collection<Commande> commandes = readJson(json);
        Planning planning = jajascript.optimize(commandes);
        assertThat(planning.getGain()).isEqualTo(86);
    }

    private Collection<Commande> readJson(final String json) {
        Collection<Commande> commandes = (Collection<Commande>) new Gson().fromJson(json, LIST_TYPE);
        if (commandes == null) {
            return new ArrayList<>();
        }
        return commandes;
    }
}
