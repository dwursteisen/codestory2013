package com.github.dwursteisen.codestory;

import com.github.dwursteisen.codestory.jajascript.Commande;
import com.github.dwursteisen.codestory.jajascript.Planning;
import com.github.dwursteisen.codestory.jajascriptv3.Jajascript;
import com.github.dwursteisen.codestory.rule.WebServerRule;
import org.fest.assertions.api.Assertions;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.jayway.restassured.RestAssured.*;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created with IntelliJ IDEA. User: david Date: 12/01/13 Time: 16:44 To change this template use File | Settings | File Templates.
 */
public class JajascriptTest {

    public static final Commande MONAD_42 = new Commande("MONAD42", 0, 5, 10);
    public static final Commande META_18 = new Commande("META18", 3, 7, 14);
    public static final Commande LEGACY01 = new Commande("LEGACY01", 5, 9, 8);
    public static final Commande YAGNI17 = new Commande("YAGNI17", 5, 9, 7);
    private static final String EXEMPLE = "[\n" + "    { \"VOL\": \"MONAD42\", \"DEPART\": 0, \"DUREE\": 5, \"PRIX\": 10 },\n"
            + "    { \"VOL\": \"META18\", \"DEPART\": 3, \"DUREE\": 7, \"PRIX\": 14 },\n"
            + "    { \"VOL\": \"LEGACY01\", \"DEPART\": 5, \"DUREE\": 9, \"PRIX\": 8 },\n"
            + "    { \"VOL\": \"YAGNI17\", \"DEPART\": 5, \"DUREE\": 9, \"PRIX\": 7 }\n" + "]";

    private static final String EXEMPLE_ACTUAL = "[{\"VOL\": \"AF514\", \"DEPART\":0, \"DUREE\":5, \"PRIX\": 10}]";
    private static final String EXEMPLE_BIS = "[\n" + "    {\"VOL\": \"AF1\", \"DEPART\":0, \"DUREE\":1, \"PRIX\": 5},\n"
            + "    {\"VOL\": \"AF2\", \"DEPART\":0, \"DUREE\":1, \"PRIX\": 6}\n" + "    ]";


    @ClassRule
    public static WebServerRule server = new WebServerRule();

    @Before
    public void setUp() {
        urlEncodingEnabled = false;
        port = server.getPort();

    }

    @Test
    public void should_call_the_servlet() {
        // same as request received
        expect().body("gain", equalTo(18)).statusCode(201).given().body(EXEMPLE).when().post("/jajascript/optimize");
        expect().body("gain", equalTo(10)).statusCode(201).given().body(EXEMPLE_ACTUAL).when().post("/jajascript/optimize");
        expect().body("gain", equalTo(6)).statusCode(201).given().body(EXEMPLE_BIS).when().post("/jajascript/optimize");
        expect().body("gain", equalTo(0)).statusCode(201).when().post("/jajascript/optimize");
    }


    @Test
    public void should_generate_a_planning_from_commandes() {
        List<Commande> commandes = Arrays.asList(MONAD_42, LEGACY01);
        Planning planning = new Jajascript().optimize(commandes);
        Assertions.assertThat(planning.getPath()).containsExactly("MONAD42", "LEGACY01");
    }

    @Test
    public void should_generate_a_planning_with_gains() {
        List<Commande> commandes = Arrays.asList(MONAD_42, META_18);
        Planning planning = new Jajascript().optimize(commandes);
        Assertions.assertThat(planning.getGain()).isEqualTo(14);
    }

    @Test
    public void should_give_the_answer() {
        List<Commande> commandes = Arrays.asList(MONAD_42, META_18, LEGACY01, YAGNI17);
        Planning planning = new Jajascript().optimize(commandes);
        assertThat(planning.getPath()).containsExactly("MONAD42", "LEGACY01");
        Assertions.assertThat(planning.getGain()).isEqualTo(18);
    }

}
