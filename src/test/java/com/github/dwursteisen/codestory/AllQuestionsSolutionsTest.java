package com.github.dwursteisen.codestory;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.port;
import static com.jayway.restassured.RestAssured.urlEncodingEnabled;
import static org.fest.assertions.api.Assertions.assertThat;

import com.github.dwursteisen.codestory.rule.WebServerRule;
import org.fest.assertions.api.Assertions;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

public class AllQuestionsSolutionsTest {

    @ClassRule
    public static WebServerRule server = new WebServerRule();

    @Before
    public void setUp() {
        urlEncodingEnabled = false;
        port = server.getPort();

    }

    @Test
    public void should_respond_to_esTuAbonneAlaMailingList() {
        assertThat(get("/?q=Es+tu+abonne+a+la+mailing+list(OUI/NON)").asString()).isEqualTo("OUI");
    }

    @Test
    public void should_respond_to_esTuHeureuxDeParticiper() {
        assertThat(get("/?q=Es+tu+heureux+de+participer(OUI/NON)").asString()).isEqualTo("OUI");
    }

    @Test
    public void should_respond_to_esTuPresARecevoirUnEnonceAuFormatMarkdown() {
        assertThat(get("/?q=Es+tu+pret+a+recevoir+une+enonce+au+format+markdown+par+http+post(OUI/NON)").asString()).isEqualTo("OUI");
    }

    @Test
    public void should_respond_OUI_to_OUI_NON_question() {
        assertThat(get("/?q=je_suis_une_nouvelle_question(OUI/NON)").asString()).isEqualTo("OUI");
    }

    @Test
    public void should_respond_non_quand_on_me_demande_si_je_repond_toujours_oui() {
        assertThat(get("/?q=Est+ce+que+tu+reponds+toujours+oui(OUI/NON)").asString()).isEqualTo("NON");
    }

    @Test
    public void should_give_mon_email() {
        assertThat(get("/?q=Quelle+est+ton+adresse+email").asString()).isEqualTo("david.wursteisen@gmail.com");
    }

    @Test
    public void should_sum_numbers() {
        assertThat(get("/?q=1+1").asString()).isEqualTo("2");
        assertThat(get("/?q=2+1").asString()).isEqualTo("3");
        assertThat(get("/?q=2+2").asString()).isEqualTo("4");
        assertThat(get("/?q=50+2").asString()).isEqualTo("52");
    }

    @Test
    public void should_sum_more_than_2_numbers() {
        assertThat(get("/?q=(1+2+3+4+5+6+7+8+9+10)*2").asString()).isEqualTo("110");
    }

    @Test
    public void should_compute_more_complicated_calculations() {
        assertThat(get("/?q=((1+2)+3+4+(5+6+7)+(8+9+10)*3)/2*5").asString()).isEqualTo("272,5");
        assertThat(get("/?q=1,5*4").asString()).isEqualTo("6");
        assertThat(get("/?q=((1,1+2)+3,14+4+(5+6+7)+(8+9+10)*4267387833344334647677634)/2*553344300034334349999000").asString()).isEqualTo(
                "31878018903828899277492024491376690701584023926880");
        assertThat(
                get(
                        "/?q=(((1,1+2)+3,14+4+(5+6+7)+(8+9+10)*4267387833344334647677634)/2*553344300034334349999000)/31878018903828899277492024491376690701584023926880")
                        .asString()).isEqualTo("1");

    }

    @Test
    public void should_product_numbers() {
        assertThat(get("/?q=1*1").asString()).isEqualTo("1");
        assertThat(get("/?q=2*1").asString()).isEqualTo("2");
        assertThat(get("/?q=2*2").asString()).isEqualTo("4");
        assertThat(get("/?q=50*2").asString()).isEqualTo("100");
        assertThat(get("/?q=1,5*1,5").asString()).isEqualTo("2,25");
    }

    @Test
    public void should_soustract_numbers() {
        assertThat(get("/?q=1-1").asString()).isEqualTo("0");
        assertThat(get("/?q=2-1").asString()).isEqualTo("1");
        assertThat(get("/?q=2-2").asString()).isEqualTo("0");
        assertThat(get("/?q=50-2").asString()).isEqualTo("48");
    }

    @Test
    public void should_divide_numbers() {
        assertThat(get("/?q=1/1").asString()).isEqualTo("1");
        assertThat(get("/?q=3/2").asString()).isEqualTo("1,5");
    }

    @Test
    public void should_respond_to_calcul_with_left_parenthesis() {
        assertThat(get("/?q=(1+2)*3").asString()).isEqualTo("9");
        assertThat(get("/?q=(1+2)+2").asString()).isEqualTo("5");
        assertThat(get("/?q=(1*2)+1").asString()).isEqualTo("3");
        assertThat(get("/?q=(1+2)/2").asString()).isEqualTo("1,5");
        assertThat(get("/?q=(1/2)+2").asString()).isEqualTo("2,5");
    }

    @Test
    public void should_respond_to_calcul_with_right_parenthesis() {
        assertThat(get("/?q=3*(1+2)").asString()).isEqualTo("9");
        assertThat(get("/?q=2+(1+2)").asString()).isEqualTo("5");
        assertThat(get("/?q=1+(1*2)").asString()).isEqualTo("3");
        assertThat(get("/?q=3/(1+2)").asString()).isEqualTo("1");
    }

    @Test
    public void should_say_yes_when_received_enonce() {
        Assertions.assertThat(get("/?q=As+tu+bien+recu+le+premier+enonce(OUI/NON)").asString()).isEqualTo("OUI");
    }

    @Test
    public void should_respond_to_calcul_with_parenthesis() {
        assertThat(get("/?q=(-1)+(1)").asString()).isEqualTo("0");
        assertThat(get("/?q=(-1)*(1)").asString()).isEqualTo("-1");
        assertThat(get("/?q=(-1)-(1)").asString()).isEqualTo("-2");
        assertThat(get("/?q=(-1)/(1)").asString()).isEqualTo("-1");

    }

    @Test
    public void should_respond_to_empty_question() {
        assertThat(get("/").asString()).isEqualTo("");
    }

    @Test
    public void should_respond_to_wrong_computation_question() {
        assertThat(get("/?q=System.getProperty('java.version')").asString()).isEqualTo("");
    }

    @Test
    public void should_respond_with_missing_question() {
        assertThat(get("/").asString()).isEqualTo("");
    }

    @Test
    public void should_respond_que_jai_connu_mieux_comme_nuit_quand_meme() {
        assertThat(get("/?q=As+tu+passe+une+bonne+nuit+malgre+les+bugs+de+l+etape+precedente(PAS_TOP/BOF/QUELS_BUGS)").asString())
                .isEqualTo("QUELS_BUGS");
    }

    @Test
    public void should_repondre_au_gros_troll_de_code_story() {
        assertThat(get("/?q=As+tu+copie+le+code+de+ndeloof(OUI/NON/JE_SUIS_NICOLAS)").asString()).isEqualTo("NON");
    }
}
