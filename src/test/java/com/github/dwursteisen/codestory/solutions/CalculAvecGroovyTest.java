package com.github.dwursteisen.codestory.solutions;

import org.fest.assertions.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: david
 * Date: 23/01/13
 * Time: 22:42
 * To change this template use File | Settings | File Templates.
 */
public class CalculAvecGroovyTest {

    private CalculAvecGroovy calculAvecGroovy;

    @Before
    public void setUp() throws Exception {
        calculAvecGroovy = new CalculAvecGroovy();
    }

    @Test
    public void should_respond() {
        assertThat(calculAvecGroovy.canRespondToThisQuestion("1 1")).isTrue();
        assertThat(calculAvecGroovy.canRespondToThisQuestion("1*1")).isTrue();
        assertThat(calculAvecGroovy.canRespondToThisQuestion("1/1")).isTrue();
        assertThat(calculAvecGroovy.canRespondToThisQuestion("1-1")).isTrue();
    }

    @Test
    public void should_give_respond() {
        Assertions.assertThat(calculAvecGroovy.giveResponse("1,5 1,5")).isEqualTo("3");
        Assertions.assertThat(calculAvecGroovy.giveResponse("1,5 1")).isEqualTo("2,5");
    }
}
