package com.github.dwursteisen.codestory;

import com.github.dwursteisen.codestory.rule.WebServerRule;
import com.jayway.restassured.response.Response;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.port;
import static org.fest.assertions.api.Assertions.assertThat;

public class EnonceSolutionsTest {
    @ClassRule
    public static WebServerRule server = new WebServerRule();

    @Before
    public void setUp() throws Exception {
        port = server.getPort();
    }

    @Test
    public void should_respond_created_on_enonce() {
        Response response = given().body("myParam=myValue").post("/enonce/1");
        String responseAsString = response.asString();
        assertThat(responseAsString).isEmpty();
        assertThat(response.getStatusCode()).isEqualTo(201);
    }
}
