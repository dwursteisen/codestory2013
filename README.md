CodeStory 2013
==============

Pour compiler le projet, les prérequis sont :

    maven 3
    java 1.7
    un peu de patience

Pour compiler le projet :

    mvn clean install

Organisation des sources :

    src/main/java -> code source java
    src/main/scala -> code source scala
    src/test/java -> tous les tests
